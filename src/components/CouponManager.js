import React, {Fragment, useState, useEffect} from 'react';
import Navbar from '../template/Navbar'
import CouponList from '../template/CouponList'
import Manager from '../template/Manager'


function CouponManager() {
  //estados
  const [coupon, setCoupon] = useState({
    clave:'',
    descuento:0,
    fecha_ini: '2021-01-01T00:00:00',
    fecha_fin: '2021-12-31T23:59:59',
    tipo_cupon:0,
    limite_uso:0,
    id_usuario: 0,
    prod_public: 0,
    prod_postul: 0,
    prod_gold: 0
  })

  const [coupons, setCoupons] = useState([]) //estado lista cupones
  
  const [listUpdated, setListUpdated] = useState(false) //actualizar al realizar un cambio
  const [checkReferido, setCheckReferido] = useState(false)
  const [checkProducto, setCheckProducto] = useState(false)

  //trae cupones
  useEffect(() => {
    const getCoupons = () => {
      fetch('http://localhost:3000/cupones/obtenerCupones')
      .then(res => res.json())
    //  .then(res => console.log(res.details))
      .then(res => setCoupons(res.details)) //trae los cupones
    }
    getCoupons()
    setListUpdated(false)
  }, [listUpdated])  

  //tablas
  return (
    <Fragment>
      <Navbar brand='Gestor de cupones'/><br/>
      <div className="Container" style={{paddingLeft:50}}>
        <div className="row">
        
          <div className="col-2" id="formManager">
            <h3 style={{textAlign: 'center'}} className='manager'>Manager</h3>
            <Manager coupon={coupon} setCoupon={setCoupon} checkReferido={checkReferido} setCheckReferido={setCheckReferido} checkProducto={checkProducto} setCheckProducto={setCheckProducto}/>
          </div>

          <div className="col-10">
            <h3 style={{textAlign: 'center'}}> Cupones existentes </h3>
            <CouponList coupon={coupon} coupons={coupons} setListUpdated={setListUpdated}/> 
          </div>

        </div>
      </div>

    </Fragment>
  );
}


export default CouponManager;

