import React, {useState} from "react";
import axios from "axios";
import '../assets/css/Login.css';
import logo from '../assets/img/logo.png';      //img
import {useNavigate} from 'react-router-dom';



function Login () {
    
    let navigate = useNavigate();

    //estado inicial
    const [datos, setDatos] = useState ({
        email:'',
        password:'',
        error:false,
        errorMsg:''
    })


//eventos
    const handleSubmit = (ingresar) => {
        ingresar.preventDefault();
    }

    const handleModif = async evento => {
        await setDatos ({
                ...datos,
                error : false, 
                errorMsg: '',  
                [evento.target.name]:evento.target.value
        })
   //     console.log(this.state.form)
    }

    const handleIngresar = () => {
        console.log("dio clic en login ")
   

        axios.post('http://localhost:3000/cuentas/login', datos)
        .then(response => {
   //         console.log(response.data.details) <switch><Route path="/" exact render = { props=> ( <App {...props}/>)}></Route><switch>
            console.log(response)

            
            if(response.data.result === 1){
                localStorage.setItem("token", response.data.details.token);
                navigate ('CouponManager')
                
                
            }else{
                setDatos({
                    ...datos, 
                    error : true,
                    errorMsg: response.data.msg
                })
            }
        }).catch( error => {
            console.log(error);
            setDatos({
                error:true,
                errorMsg: "Error al conectar"
            })
        })
    }


        return(
            <React.Fragment>
                <div className="wrapper fadeInDown">
                    <div id="formContent">

                        <div className="fadeIn first">
                            <img src={logo} alt="User Icon" />
                        </div>

                        <form onSubmit={handleSubmit}>
                            <input type="text" className="fadeIn second" name="email" placeholder="Correo electrónico" onChange={handleModif}/>
                            <input type="password" className="fadeIn third" name="password" placeholder="Contraseña" onChange={handleModif}/>
                            <input type="submit" className="fadeIn fourth" value="INGRESAR" onClick={handleIngresar}/>
                        </form>

{/*                        <div id="formFooter">
                            <a className="underlineHover" href="#!">¿Olvidaste tu contraseña?</a>
                       </div>
        */}
                    {datos.error === true &&
                        <div className="msg" role="alert">
                            {datos.errorMsg}
                        </div>
                    }

                    </div>
                </div>
            </React.Fragment>
            );
       
}

export default Login;