import React from 'react';
import Login from './components/Login';
import CouponManager from './components/CouponManager';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom'


function App(){


  return (
    <React.Fragment>
      <Router>
        <Routes>
          <Route path="/" element={<Login/>}/>
          <Route path="/couponmanager" element={<CouponManager/>}/>
          
        </Routes>
      </Router>
    </React.Fragment>

  );
}

export default App;





/*
import React, {Fragment, useState, useEffect} from 'react';
import Navbar from './components/Navbar'
import CouponList from './components/CouponList'
import Manager from './components/Manager'

function App() {
  //estados
  const [coupon, setCoupon] = useState({
    clave:'',
    descuento:0,
    fecha_ini: '2021-01-01T00:00:00',
    fecha_fin: '2021-12-31T23:59:59',
    tipo_cupon:0,
    limite_uso:0,
    id_usuario: 0
  })

  const [coupons, setCoupons] = useState([]) //estado lista cupones
  
  const [listUpdated, setListUpdated] = useState(false) //actualizar al realizar un cambio
  const [checkReferido, setCheckReferido] = useState(false)


  //trae cupones
  useEffect(() => {
    const getCoupons = () => {
      fetch('http://localhost:3000/cupones/obtenerCupones')
      .then(res => res.json())
    //  .then(res => console.log(res.details))
      .then(res => setCoupons(res.details)) //trae los cupones
    }
    getCoupons()
    setListUpdated(false)
  }, [listUpdated])  

  //tablas
  return (
    <Fragment>
      <Navbar brand='Gestor de cupones'/><br/>
      <div className="container">
        <div className="row">
        
          <div className="col-3">
            <h3 style={{textAlign: 'center'}}>Manager</h3>
            <Manager coupon={coupon} setCoupon={setCoupon} checkReferido={checkReferido} setCheckReferido={setCheckReferido}/>
          </div>

          <div className="col-7">
            <h3 style={{textAlign: 'center'}}> Cupones existentes </h3>
            <CouponList coupon={coupon} coupons={coupons} setListUpdated={setListUpdated}/> 
          </div>

        </div>
      </div>

    </Fragment>
  );
}


export default App;
*/


