import React from "react";
import moment from "moment";

const CouponList = ({coupons, setListUpdated}) => {

    // Función cambiar estatus activo
    const handleCambiarStatus = (clave, estatus_activo, nuevoEstado) => { 
        var estadoActual = estatus_activo                
                //Definición de nuevo estado
            if (estadoActual ==='ACT'){
                nuevoEstado=0
            //    console.log('act si', clave, ' nvo ', nuevoEstado)                
            }else{
                nuevoEstado=1
            //    console.log('act else', clave,' nvo ', nuevoEstado)
            }

        const requestInit={
            method:'PUT',
            headers: {'content-Type': 'application/json'},
            body: JSON.stringify({
                clave: clave,
                estado_activo: nuevoEstado})
        }

        fetch('http://localhost:3000/cupones/actualizarEstadoActivo/', requestInit)
        .then(res => res.text())
        .then(res => console.log(res))

        setListUpdated(true)
    }


    return(
        <table className="container">
            <thead>
                <tr> 
                    <th>ID</th>
                    <th>Clave</th>
                    <th>Descuento</th>
                    <th>Fecha ini</th>
                    <th>Fecha fin</th>
                    <th>Estatus</th>
                    <th>Tipo</th>                  
                    <th>Límite de uso</th>
                    <th>Nombre Ref.</th>
                    <th>Email Ref.</th> 
                </tr>
            </thead>
            
            <tbody>
                {coupons.map(coupon => (
                    <tr key={coupon.id_cupon}> 
                        <td>{coupon.id_cupon}</td>
                        <td>{coupon.clave}</td>
                        <td>{coupon.descuento}</td>
                        <td>{moment(coupon.fecha_ini).format('DD-MM-yyyy h:mm:ss a')}</td>
                        <td>{moment(coupon.fecha_fin).format('DD-MM-yyyy h:mm:ss a')}</td>
                        <td>{coupon.estatus_activo}</td>
                        <td>{coupon.tipo_cupon}</td>
                        <td>{coupon.limite_uso}</td>
                        <td>{coupon.nombre_usuario}</td>
                        <td>{coupon.email}</td>
                        <td>
                            <div className="mb-1">
                               <button onClick={() => handleCambiarStatus(coupon.clave, coupon.estatus_activo)} className="bton btn-dark">Activar/Desactivar</button>

                            </div>
                        </td>
                    </tr>
                ))}

            </tbody>
        </table>
    );
    
}

export default CouponList;