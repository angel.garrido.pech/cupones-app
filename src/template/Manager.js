import React from 'react';
import '../assets/css/Manager.css';

const Manager = ({coupon, setCoupon, checkReferido, setCheckReferido, checkProducto, setCheckProducto}) => {
    //funciones
    const handleModif = evento => {   //guarda el estado de modificación
        setCoupon({
            ...coupon, //estado anterior           
            [evento.target.name]:evento.target.value         
        })
    }

    let{clave, descuento, fecha_ini, fecha_fin, tipo_cupon, limite_uso, id_usuario, prod_public, prod_postul, prod_gold} = coupon

    const handleCheckRef = (tipo_cupon) =>{
        tipo_cupon = parseInt(tipo_cupon)

        if (tipo_cupon === 2) {
            setCheckReferido(true)
        } else {
            setCheckReferido(false)                        
        }
    }

    //validar que sea tipo producto para habilitar checkbox
    const handleCheckProd = (tipo_cupon) =>{
        tipo_cupon = parseInt(tipo_cupon)

        if (tipo_cupon === 3) {
            setCheckProducto(true)
        } else {
            setCheckProducto(false)                        
        }
    }


    const handleNoRef = (tipo_cupon) => {
        tipo_cupon = parseInt(tipo_cupon)
        if (tipo_cupon !== 2){
            setCoupon({...coupon, limite_uso:0, id_usuario:0})
        }
    }

    const  handleSubmit = () => {  //post
        descuento = parseFloat(descuento)
        limite_uso = parseInt(limite_uso,10)
        id_usuario = parseInt(id_usuario,10)
        tipo_cupon = parseInt(tipo_cupon)
            //validación de datos
       
        if (tipo_cupon === 2){
            if (clave === ''|| descuento === '' || descuento === 0 || fecha_ini === '' || fecha_fin === '' || limite_uso === '' || id_usuario === ''){
                alert('Todos los campos son obligatorios ' + tipo_cupon)
                return
            }
        }
        else {
            if (clave === ''|| descuento === '' || descuento === 0 || fecha_ini === '' || fecha_fin === ''){
                alert('Debe informar los campos obligatorios ' + tipo_cupon + ' us ' + id_usuario)
                return
            }
        }

        console.log(coupon)
        alert('cupon ' + coupon)

        const requestInit={
            method:'POST',
            headers: {'content-Type': 'application/json'},
            body: JSON.stringify(coupon)
        }

        console.log(requestInit)
        alert(requestInit)
        // consulta crearCupon
        fetch('http://localhost:3000/cupones/crearCupon', requestInit)
        .then(res => res.json())
        .then(res => console.log(res.details))
       
        //limpia el form y reinicia el state
        setCoupon({
            clave:'',
            descuento:0,
            fecha_ini: '2021-01-01T00:00:00',
            fecha_fin: '2021-12-31T23:59:59',
            tipo_cupon:0,
            limite_uso:0,
            id_usuario:0,
            prod_public: 0,
            prod_postul: 0,
            prod_gold: 0
        })
      }

    //form
    return ( 
        <form onSubmit={handleSubmit}>
            <div className="mb-3">
                <label htmlFor="clave" className="manager">Clave</label>
                <input name="clave" onChange={handleModif} type="text" id="clave" className="manager" value={clave}/>
            </div>

            <div className="mb-3">
                <label htmlFor="descuento" className="manager">Descuento (%)</label>
                <input name="descuento" onChange={handleModif} type="text" id="descuento" className="manager" step="0.00" value={descuento}/>
            </div>

            <div className="mb-3">
                <label htmlFor="fecha_ini" className="manager">Fecha de inicio</label>
                <input name="fecha_ini" onChange={handleModif} type="datetime-local" id="fecha_ini" className="manager" value={fecha_ini}/>
            </div>

            <div className="mb-3">
                <label htmlFor="fecha_fin" className="manager">Fecha de finalización</label>
                <input name="fecha_fin" onChange={handleModif} type="datetime-local" id="fecha_fin" className="manager" value={fecha_fin}/>
            </div><br/>
            <hr/>
            <div className="mb-3">    
                <label className="manager">Tipo de cupón</label> <br/>
                <input className="manager" name="tipo_cupon" onChange={handleModif} type="radio" id="tipo_cupon_0" value="0" defaultChecked onClick={() => handleCheckRef(0)}/>
                <label htmlFor="tipo_cupon_0" className='manager-radio'>Nuevo</label>
                <input className="manager" name="tipo_cupon" onChange={handleModif} type="radio" id="tipo_cupon_1" value="1" onClick={() => handleCheckRef(1)}/>
                <label htmlFor="tipo_cupon_0" className='manager-radio'>Partner</label>
                <input className="manager" name="tipo_cupon" onChange={handleModif} type="radio" id="tipo_cupon_2" value="2" onClick={() => handleCheckRef(2)}/>
                <label htmlFor="tipo_cupon_0" className='manager-radio'>Referido</label>
                <input className="manager" name="tipo_cupon" onChange={handleModif} type="radio" id="tipo_cupon_3" value="3" onClick={() => handleCheckRef(3)}/>
                <label htmlFor="tipo_cupon_0" className='manager-radio'>Producto</label>  
            </div>
            <hr/>
            <br/>
            <div className="mb-3">
                <label htmlFor="limite" className="manager">Límite de uso</label>
                <input name="limite_uso" onChange={handleModif} type="number" id="limite" className="manager" value={limite_uso} disabled={!checkReferido}/>
            </div>

            <div className="mb-3">
                <label htmlFor="referido" className="manager">ID usuario referido</label>
                <input name="id_usuario" onChange={handleModif} type="number" id="referido" className="manager" value={id_usuario} disabled={!checkReferido}/>
            </div>

            <div className="mb-3">    
                <label className="manager-sec">Productos válidos</label> <br/>
                <input className="manager" name="prod_public" onChange={handleModif} type="checkbox" id="prod_public" value="1" onClick={() => handleCheckRef(3)} disabled={!checkProducto}/>
                <label htmlFor="prod_public" className='manager-radio'>Publicaciones</label>
                <input className="manager" name="prod_postul" onChange={handleModif} type="checkbox" id="prod_postul" value="1" onClick={() => handleCheckRef(3)} disabled={!checkProducto}/>
                <label htmlFor="prod_postul" className='manager-radio'>Postulaciones</label>
                <input className="manager" name="prod_gold" onChange={handleModif} type="checkbox" id="prod_gold" value="1" onClick={() => handleCheckRef(3)} disabled={!checkProducto}/>
                <label htmlFor="prod_gold" className='manager-radio'>Gold</label>
            </div>
            
            <input type="submit" className="wrapper manager" onClick = {() => handleNoRef(coupon.tipo_cupon)} value="Crear"/>
            
        </form>
     );
}
 
export default Manager;
