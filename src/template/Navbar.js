import React from 'react';
import '../assets/css/Navbar.css'

const Navbar = ({brand}) => {
    return(
        <nav className="topnav">
            <div className="container">
                <a href="#!" className="topnav">{brand}</a>
            </div>
        </nav>
    );
}

export default Navbar;